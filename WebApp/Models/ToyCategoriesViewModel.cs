﻿using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Models
{
    public class ToyCategoriesViewModel
    {
        public List<ToyCategory> Categories { get; set; }
        public List<int> SelectedCategories { get; set; } = new List<int>();
        public List<int> SelectedSubCategories { get; set; } = new List<int>();
        public List<int> SelectedSubSubCategories { get; set; } = new List<int>();
    }
}
