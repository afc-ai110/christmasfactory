﻿namespace Fr.EQL.AI110.ChristmasFactory.Web.Models
{
    public class StepsViewModel
    {
        public List<String> Categories { get; set; } = new List<string>();

        public List<String> SousCategories { get; set; } = new List<string>();
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }



    }
}
