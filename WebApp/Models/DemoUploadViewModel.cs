﻿namespace Fr.EQL.AI110.ChristmasFactory.Web.Models
{
    public class DemoUploadViewModel
    {
        // La property qui récupère le fichier uploadé (de type IFormFile) :
        public IFormFile MonFichier { get; set; }

    }
}