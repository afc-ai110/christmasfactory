﻿namespace WebApp.Models
{
    public class VoyageLointain
    {
        public string Destination { get; set; }
        public List<string> Activites { get; set; }
        public int Duree { get; set; }

        public VoyageLointain()
        {
            this.Activites = new List<string>();
        }

    }
}
