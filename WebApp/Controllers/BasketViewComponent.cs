﻿using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    // Déclaration d'un ViewComponent (= controlleur)
    public class BasketViewComponent : ViewComponent
    {
        // un ViewComponent contient forcément un méthode Invoke
        public IViewComponentResult Invoke(int qte)
        {
            // déclaration du model :
            int nbArticles = qte; // en vrai je vais chercher l'info dans business
            
            // la vue se trouve dans Views/Shared/Components/Basket/Default.cshtml
            return View(nbArticles);
        }
    }
}
