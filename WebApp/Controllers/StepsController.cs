﻿using Fr.EQL.AI110.ChristmasFactory.Business;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class StepsController : Controller
    {

        public IActionResult Step1()
        {
            StepsViewModel model = new StepsViewModel();

            model.Categories.Add("Toto");
            model.Categories.Add("Titi");
            model.Categories.Add("Tutu");

            return View(model);
        }

        [HttpPost]
        public IActionResult Step2(StepsViewModel model)
        {
            

            return View(model);
        }

    }
}
