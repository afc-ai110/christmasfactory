﻿using Fr.EQL.AI110.ChristmasFactory.Business;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;


namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class ElfController : Controller
    {
        private IActionResult EditerElfe(string rubrique, Elf elf)
        {
            ViewBag.Rubrique = rubrique;
            return View("Edit", elf);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            ElfBusiness bu = new ElfBusiness();
            Elf elf = bu.GetElfById(id);

            return EditerElfe("Modification d'un elfe", elf);
        }

        [HttpPost]
        public IActionResult Update(Elf elf)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ElfBusiness bu = new ElfBusiness();
                    bu.SaveElf(elf);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return EditerElfe("Modification d'un elfe", elf);
                }
            }
            else
            {
                return EditerElfe("Modification d'un elfe", elf);
            }
        }

        // affichage d'un elfe avec id dans la route :
        public IActionResult Infos(int id)
        {
            ElfBusiness bu = new ElfBusiness();
            Elf elf = bu.GetElfById(id);
            return View("Details", elf);
        }


        // affichage d'un elfe avec id en querystring :
        public IActionResult Details()
        {
            // recupération de l'id (QuesryString)
            int id = int.Parse(Request.Query["id"]);

            ElfBusiness bu = new ElfBusiness();
            Elf elf = bu.GetElfById(id);
            // Mock (bouchon)
            //Elf elf = new Elf();            
            //elf.Name = "Raoul";
            //elf.Id = 42;
            //elf.IsAvailable = true;
            //elf.Picture = "raoul.jpg";

            return View(elf);
        }


        // attribute httpGet
        [HttpGet] // action sur GET
        public IActionResult Create()
        {
            return EditerElfe("Création d'un elfe", null);
            
            //ViewBag.Rubrique = "Création d'un elfe";
            //return View("Edit");
        }

        // attribute httpPost
        [HttpPost] // action sur POST
        public IActionResult Create(Elf elf)
        {
            // j'enregistre mon Elfe uniquement s'il respecte
            // les regles de validation :
            if (ModelState.IsValid)
            {
                ElfBusiness bu = new ElfBusiness();
                bu.SaveElf(elf);
                return RedirectToAction("Index");
            }
            else
            {
                // si pas valide, j'affiche à nouveau la vue :
                return EditerElfe("Création d'un elfe", elf);
            }
        }

        public IActionResult Index()
        {
            try
            {
                ElfBusiness bu = new ElfBusiness();
                List<ElfDetails> elves = bu.SearchElves();
                return View(elves);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("Error");
            }
        }

        #region GESTION GET/POST MANUELLE
        public IActionResult Nouveau()
        {
            if (Request.Method == "GET")
            {
                return View();
            }
            else
            {
                // je récupère les informations
                // postées dans la requete :
                string nom = Request.Form["nom"];
                string picture = Request.Form["picture"];
                string dispo = Request.Form["dispo"];

                // je fabrique un objet Elf :
                Elf elf = new Elf();
                elf.Name = nom;
                elf.Picture = picture;
                elf.IsAvailable = dispo == "on" ? true : false;

                // je l'enregistre :
                ElfBusiness bu = new ElfBusiness();
                bu.SaveElf(elf);

                return Content("Merci...");
            }
        }
        #endregion
    }
}
