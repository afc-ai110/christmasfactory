﻿using Fr.EQL.AI110.ChristmasFactory.Business;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class ToyController : Controller
    {
        [HttpPost]
        public IActionResult ExempleCategories(ToyCategoriesViewModel model)
        {
            return View(model); 
        }

        [HttpGet]
        public IActionResult ExempleCategories()
        {
            ToyBusiness bu = new ToyBusiness();
            ToyCategoriesViewModel model = new ToyCategoriesViewModel();
            model.Categories = bu.GetCategories(null); 
            return View(model);  
        }

        private IActionResult EditerToy(Toy toy)
        {
            ElfBusiness elfBu = new ElfBusiness();
            List<Elf> elves = elfBu.GetElves();
            ViewBag.ListeResponsables = elves;
            return View("Edit", toy);
        }

        [HttpGet]
        [Route("Nouveau-Jouet")]
        public IActionResult Create()
        {
            ViewData["reponse"] = 42;
            ViewBag.Formateur = "Benoit";

            return EditerToy(null);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            // /toy/update/42
            ToyBusiness tbu = new ToyBusiness();
            Toy toy = tbu.GetToyById(id);

            return EditerToy(toy);
        }

        public IActionResult Delete(int id)
        {
            ToyBusiness bu = new ToyBusiness();
            bu.DeleteToy(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Update(Toy toy)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ToyBusiness tbu = new ToyBusiness();
                    tbu.SaveToy(toy);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
            }
            else
            {
                return EditerToy(toy);
            }
        }

        [HttpPost]
        [Route("Nouveau-Jouet")]
        public IActionResult Create(Toy toy)
        {
            ToyBusiness bu = new ToyBusiness();
            bu.SaveToy(toy);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Index(ToySearchViewModel model)
        {
            ToyBusiness toyBU = new ToyBusiness();
            model.Toys = toyBU.SearchToys(model.IdResponsable, model.ToyName, model.MinPrice, model.MaxPrice);

            ElfBusiness elfBU = new ElfBusiness();
            model.Elves = elfBU.GetElves();

            return View(model);
        }

        [HttpGet]
        public IActionResult Index()
        {
            // récupérer le modele
            ToySearchViewModel model = new ToySearchViewModel();
            
            ToyBusiness toyBU = new ToyBusiness();
            model.Toys = toyBU.SearchToys(0, "", 0, 0);

            ElfBusiness elfBU = new ElfBusiness();
            model.Elves = elfBU.GetElves();

            // charger la vue :
            return View(model);
        }
    }
}
