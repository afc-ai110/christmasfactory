﻿using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ChristmasController : Controller
    {
        // Exemple utilisation du queryString
        public IActionResult ExempleQS()
        {
            // /christmas/exempleqs?word=hello&nb=42

            // récupérer les paramètres :
            string word = Request.Query["word"];
            int nb = int.Parse(Request.Query["nb"]);
            
            string traduction = "";

            for (int i = 0; i < nb; i++)
            {
                traduction += word + "<br/>";
            }
            Response.ContentType = "text/html";
            return Content(traduction); 
        }

        public IActionResult Welcome()
        {
            return View();
        }

        public IActionResult Contact()
        {
            // si ma requete est en get
            if (Request.Method == "GET")
            {
                // je charge la vue par défaut :
                return View();
            }
            else
            {
                // je dois récupérer les données postées : Request.Form
                string email = Request.Form["email"];
                string message = Request.Form["message"];
                // je spécifie explicitement le type de contenu :
                Response.ContentType = "text/html;charset=utf-8";
                // j'écris "à la main" le corps de la réponse :
                return Content("<h1>Message bien reçu...</h1>"
                                + "message : " + message 
                                + "<br/> mail : " + email);
            }
        }

        public IActionResult Voyage()
        {
            // je récupère un modèle :
            VoyageLointain v = new VoyageLointain();

            v.Destination = "Japon";
            v.Duree = 11;
            v.Activites.Add("shopping");
            v.Activites.Add("tatouages");
            v.Activites.Add("restaurant");
            
            // j'appelle la vue en lui fournissant le modèle :
            return View(v);
        }

    }
}
