﻿using Fr.EQL.AI110.ChristmasFactory.Web.Models;
using Microsoft.AspNetCore.Mvc;


namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class UploadController : Controller
    {
        public IActionResult Demo( )
        {  
            return View();
        }


        // Action post pour le formulaire : doit être Asynchrone
        [HttpPost]
        public async Task<IActionResult> Demo(DemoUploadViewModel model)
        {
            // je récupère le fichier uploadé dans mon viewModel :
            IFormFile file = model.MonFichier;

            // validation du ficier reçu (format, taile...)
            if (file == null || file.Length == 0)
                return Content("file not selected");

            // je spécifie l'emplacement de destination sur le serveur :
            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot\\images\\upload",
                        file.FileName);

            // j'enregistre le fichier
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            // redirection vers une vue...
            return RedirectToAction("Demo");
        }
    }
}
