﻿using Fr.EQL.AI110.ChristmasFactory.DataAccess;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Business
{
    public class ToyBusiness
    {
        public List<ToyCategory> GetCategories(int? parentId)
        {
            ToyCategoryDAO dao = new ToyCategoryDAO();
            return dao.GetCategories(parentId);
        }

        public List<Toy> GetToys()
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetAll();
        }

        public List<ToyDetails> SearchToys(int idResponsable, string name, int minPrice, int maxPrice)
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetByMultiCriteria(idResponsable, name, minPrice, maxPrice);
        }

        public void SaveToy(Toy toy)
        {
            if (toy.Price < Toy.MIN_PRICE || toy.Price > Toy.MAX_PRICE)
            {
                throw new Exception("Le prix doit être compris entre " 
                                    + Toy.MIN_PRICE + " et " + Toy.MAX_PRICE);
            }

            ToyDAO toyDAO = new ToyDAO();

            if (toy.Id == 0)
            {
                toyDAO.Insert(toy);
            }
            else
            {
                toyDAO.Update(toy);
            }
        }

        public Toy GetToyById(int id)
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetById(id);
        }

        public void DeleteToy(int id)
        {
            ToyDAO dao = new ToyDAO();
            dao.Delete(id);
        }
    }
}
