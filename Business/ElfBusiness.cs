﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.ChristmasFactory.DataAccess;
using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.Business
{
    public class ElfBusiness
    {
        public void SaveElf(Elf elf)
        {
            // appliquer les règles de gestion
            if (elf.Name.Length < Elf.NAME_MIN_LENGTH)
            {
                throw new Exception("Création impossible : le nom doit faire au moins " + Elf.NAME_MIN_LENGTH + " caractères");
            }

            ElfDAO dao = new ElfDAO();
            if (elf.Id == 0)
            {
                // creation :
                dao.Insert(elf);
            }
            else
            {
                // mise à jour : 
                dao.Update(elf);
            }
        }
       
        public Elf GetElfById(int id)
        {
            //return new ElfDAO().GetById(id);    
            ElfDAO dao = new ElfDAO();
            return dao.GetById(id);
        }

        public List<Elf> GetElves()
        {
            ElfDAO dao = new ElfDAO();
            return dao.GetAll();
        }

        public List<ElfDetails> SearchElves()
        {
            ElfDAO dao = new ElfDAO();
            return dao.GetAllWithDetails();
        }

    }
}
