﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class ToyDetails : Toy
    {
        public ToyDetails(Toy toy)
        {
            Id = toy.Id;
            Name = toy.Name;
            Description = toy.Description;
            Price = toy.Price;
            IdResponsable = toy.IdResponsable;
        }

        public string RespName { get; set; }
    }
}
