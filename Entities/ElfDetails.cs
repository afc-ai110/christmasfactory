﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class ElfDetails : Elf
    {
        public string CategoryName { get; set; }

        public ElfDetails(Elf elf)
        {
            this.Id = elf.Id;
            this.Name = elf.Name;
            this.IsAvailable = elf.IsAvailable;
            this.Picture = elf.Picture;
            this.IdCategory = elf.IdCategory;
        }

    }
}
