﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class Elf
    {
        public const int NAME_MIN_LENGTH = 3;

        #region ATTRIBUTES
        public int Id { get; set; }

        [Required(ErrorMessage = "champ nom obligatoire")]
        [MinLength(NAME_MIN_LENGTH, ErrorMessage = "taille de champ incorrecte")]
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
        public string Picture { get; set; }
        public int IdCategory { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Elf()
        {
        }

        public Elf(int id, string name, bool isAvailable, string picture)
        {
            Id = id;
            Name = name;
            IsAvailable = isAvailable;
            Picture = picture;
        }
        #endregion

        #region METHODS

        public override string? ToString()
        {
            return this.Id + " - " + this.Name;
        }

        #endregion
    }
}
