﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class Toy
    {
        public const int MIN_PRICE = 0;
        public const int MAX_PRICE = 100;

        #region ATTRIBUTES
        public int Id { get; set; }

        [Required(ErrorMessage = "Le Nom est obligatoire")]
        public string Name { get; set; }
        public string Description { get; set; }
        
        [Range(MIN_PRICE, MAX_PRICE, ErrorMessage = "Le prix est en dehors des limites")]
        public float Price { get; set; }
        // nullable int :
        public int? IdResponsable { get; set; }

        #endregion

        #region CONSTRUCTORS
        public Toy()
        {

        }
        public Toy(int id, string name, string description, float price, int idResponsable)
        {
            Id = id;
            Name = name;
            Description = description;
            Price = price;
            IdResponsable = idResponsable;
        }

        #endregion
    }
}
