﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class ToyCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }

        public List<ToyCategory> Children { get; set; } = new List<ToyCategory>();
    }
}
