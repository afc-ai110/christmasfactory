﻿using Fr.EQL.AI110.ChristmasFactory.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    public class ToyDAO : DAO
    {
        public List<Toy> GetAll()
        {
            List<Toy> result = new List<Toy>();

            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * FROM toy";
            
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Toy toy = DataReaderToEntity(dr);

                    result.Add(toy);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(
                    "Erreur lors de la récupération des jouets : "
                    + ex.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            
            return result;
        }

        public List<ToyDetails> GetByMultiCriteria(int idResponsable, string name, int minPrice, int maxPrice)
        {
            List<ToyDetails> result = new List<ToyDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT t.*, e.name 'responsable' 
                                FROM toy t
                                LEFT JOIN elf e ON t.id_responsable = e.id
                                WHERE (id_responsable = @idResponsable OR @idResponsable = 0)
                                AND (price >= @minPrice OR @minPrice = 0)
                                AND (price <= @maxPrice OR @maxPrice = 0)
                                AND (t.name LIKE @name)";

            name = "%" + name + "%";
            cmd.Parameters.Add(new MySqlParameter("name", name));
            cmd.Parameters.Add(new MySqlParameter("idResponsable", idResponsable));
            cmd.Parameters.Add(new MySqlParameter("minPrice", minPrice));
            cmd.Parameters.Add(new MySqlParameter("maxPrice", maxPrice));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Toy toy = DataReaderToEntity(dr);
                    ToyDetails toyDetails = new ToyDetails(toy);

                    if (!dr.IsDBNull(dr.GetOrdinal("responsable")))
                    {
                        toyDetails.RespName = dr.GetString(dr.GetOrdinal("responsable"));
                    }

                    result.Add(toyDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(
                    "Erreur lors de la récupération des jouets : "
                    + ex.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }

            return result;
        }

        public void Delete(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE FROM toy
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de suppression d'un jouet : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public void Update(Toy toy)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE toy 
                                SET name = @name, 
                                    description = @description, 
                                    price = @price, 
                                    id_responsable = @id_responsable
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("name", toy.Name));
            cmd.Parameters.Add(new MySqlParameter("description", toy.Description));
            cmd.Parameters.Add(new MySqlParameter("price", toy.Price));
            cmd.Parameters.Add(new MySqlParameter("id_responsable", toy.IdResponsable));
            cmd.Parameters.Add(new MySqlParameter("id", toy.Id));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de mise à jour d'un jouet : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public Toy GetById(int id)
        {
            Toy toy = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM toy WHERE id = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    toy = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'un jouet : " + exc.Message);   
            }
            finally
            {
                cnx.Close();
            }

            return toy;
        }

        public void Insert(Toy toy)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO toy 
                                (name, description, price, id_responsable)
                                VALUES
                                (@name, @description, @price, @id_responsable)";

            cmd.Parameters.Add(new MySqlParameter("name", toy.Name));
            cmd.Parameters.Add(new MySqlParameter("description", toy.Description));
            cmd.Parameters.Add(new MySqlParameter("price", toy.Price));
            cmd.Parameters.Add(new MySqlParameter("id_responsable", toy.IdResponsable));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch(MySqlException exc) 
            {
                throw new Exception("Erreur de creation d'un jouet : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        private Toy DataReaderToEntity(DbDataReader dr)
        {
            Toy toy = new Toy();

            toy.Id = dr.GetInt32(dr.GetOrdinal("id"));
            toy.Name = dr.GetString(dr.GetOrdinal("name"));
            toy.Description = dr.GetString(dr.GetOrdinal("description"));
            toy.Price = dr.GetFloat(dr.GetOrdinal("price"));

            if (!dr.IsDBNull(dr.GetOrdinal("id_responsable")))
            {
                // Le getOrdinal doit contenir le nom de la colonne renvoyé par MySQL
                toy.IdResponsable = dr.GetInt32(dr.GetOrdinal("id_responsable"));
            }

            return toy;
        }

    }
}
