﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Data.Common;

using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    // en java on utilise "extends"
    // en C# on utilise ":"
    public class ElfDAO : DAO
    {
        public void Insert(Elf elf)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection
            // :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO elf 
                                (name, is_available, picture) 
                                VALUES 
                                (@name, @isAvailable, @picture)";

            cmd.Parameters.Add(new MySqlParameter("name", elf.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", elf.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", elf.Picture));

            try
            {
                // 3 - ouvrir la connection :
                cnx.Open();

                // 4 - Exécuter la commande :
                int nbLignes = cmd.ExecuteNonQuery();

                // 6 - fermer la conneciton :
                cnx.Close();
            }
            catch(MySqlException exc)
            {
                throw new Exception("Erreur lors de l'insertion d'un elfe. Details : " 
                                        + exc.Message);
            }
        }

        public void Update(Elf elf)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE 
                                    elf 
                                SET name = @name,
                                    is_available = @isAvailable,
                                    picture = @picture
                                WHERE 
                                    id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", elf.Id));
            cmd.Parameters.Add(new MySqlParameter("name", elf.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", elf.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", elf.Picture));

            // 3 - ouvrir la connection :
            cnx.Open();

            // 4 - Exécuter la commande :
            int nbLignes = cmd.ExecuteNonQuery();

            // 5 - Traiter le résultat :
            Console.WriteLine(nbLignes + " ligne mise à jour");

            // 6 - fermer la connection :
            cnx.Close();
        }

        public void Delete(int id)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE FROM elf 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            // 3 - ouvrir la connection :
            cnx.Open();

            // 4 - Exécuter la commande :
            int nbLignes = cmd.ExecuteNonQuery();

            // 5 - Traiter le résultat :
            Console.WriteLine(nbLignes + " ligne supprimée");

            // 6 - fermer la connection :
            cnx.Close();
        }

        public Elf GetById(int id)
        {
            Elf result = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM elf WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                result = DataReaderToEntity(dr);
            }

            cnx.Close();

            return result;
        }

        public List<Elf> GetAll()
        {
            //             java vs    C#
            // interface : List       IList 
            // classe :    ArrayList  List
            List<Elf> result = new List<Elf>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM elf";

            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Elf elf = DataReaderToEntity(dr);
                result.Add(elf);
            }

            cnx.Close();   
            
            return result;
        }

        public List<ElfDetails> GetAllWithDetails()
        {
            List<ElfDetails> result = new List<ElfDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT e.*, c.name category_name 
                                FROM elf e 
                                LEFT JOIN elf_category c ON e.id_category = c.id";

            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Elf elf = DataReaderToEntity(dr);
                ElfDetails elfDetails = new ElfDetails(elf);

                if (!dr.IsDBNull(dr.GetOrdinal("category_name")))
                {
                    elfDetails.CategoryName = dr.GetString(dr.GetOrdinal("category_name"));
                }

                result.Add(elfDetails);
            }

            cnx.Close();

            return result;
        }

        private Elf DataReaderToEntity(DbDataReader dr)
        {
            Elf elf = new Elf();

            elf.Id = dr.GetInt32(dr.GetOrdinal("id"));
            elf.Name = dr.GetString(dr.GetOrdinal("name"));
            elf.IsAvailable = dr.GetBoolean(dr.GetOrdinal("is_available"));
            if (!dr.IsDBNull(dr.GetOrdinal("picture")))
            {
                elf.Picture = dr.GetString(dr.GetOrdinal("picture"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("id_category")))
            {
                elf.IdCategory = dr.GetInt32(dr.GetOrdinal("id_category"));
            }

            return elf;
        }
    }
}
