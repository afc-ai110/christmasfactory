﻿using Fr.EQL.AI110.ChristmasFactory.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    public class ElfCategoryDAO : DAO
    {
        public List<ElfCategory> GetAll()
        {
            List<ElfCategory> result = new List<ElfCategory>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM elf_category";

            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                ElfCategory cat = new ElfCategory();

                cat.Id = dr.GetInt32(dr.GetOrdinal("id"));
                cat.Name = dr.GetString(dr.GetOrdinal("name"));
             
                result.Add(cat);
            }

            cnx.Close();

            return result;
        }

    }
}
