﻿using Fr.EQL.AI110.ChristmasFactory.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    public class ToyCategoryDAO : DAO
    {
        public List<ToyCategory> GetCategories(int? parentId, int level = 1)
        {
            List<ToyCategory> result = new List<ToyCategory> ();

            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();
            
            cmd.CommandText = @"SELECT * 
                                FROM toy_category 
                                WHERE (id_parent = @parentId) 
                                        OR (@parentId IS NULL AND id_parent IS NULL)";

            cmd.Parameters.Add(new MySqlParameter("parentId", parentId));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    ToyCategory category = new ToyCategory();  
                    category.Id = dr.GetInt32(dr.GetOrdinal("id"));
                    category.Name = dr.GetString(dr.GetOrdinal("name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("id_parent")))
                    {
                        category.ParentId = dr.GetInt32(dr.GetOrdinal("id_parent"));
                    }

                    result.Add(category);
                }

                cnx.Close();

                level++;
                if (level < 4)
                {
                    foreach (ToyCategory toyCategory in result)
                    {
                        toyCategory.Children = GetCategories(toyCategory.Id, level);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
    }
}
