﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fr.EQL.AI110.ChristmasFactory.Business;
using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.TestApp
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //AfficherTous();
            TestInsertion();
            AfficherDetails();
            //TestMiseAJour();
            //TestSuppression();
        }

        public static void AfficherDetails()
        {
            Console.WriteLine("id à afficher ?");
            int id = int.Parse(Console.ReadLine());
            
            ElfBusiness bu = new ElfBusiness();

            Elf elf = bu.GetElfById(id);

            if (elf != null)
            {
                Console.WriteLine(elf.Name);
                Console.WriteLine(elf.Id);
                Console.WriteLine(elf.Picture);
                Console.WriteLine(elf.IsAvailable ? "dispo" : "indispo");
            }
            else
            {
                Console.WriteLine("l'id demandé n'existe pas");
            }
        }

        public static void AfficherTous()
        {
            //ElfDAO dao = new ElfDAO();
            //List<Elf> elves = dao.GetAll();

            //// java : for (Elf elf : elves)
            //foreach (Elf elf in elves)
            //{
            //    Console.WriteLine(elf);
            //}
        }

        public static void TestSuppression()
        {
            //Console.WriteLine("id à supprimer ?");
            //int id = int.Parse(Console.ReadLine());
            //ElfDAO dao = new ElfDAO();
            //dao.Delete(id);
        }

        public static void TestMiseAJour()
        {
            //Console.WriteLine("id à modifier ?");
            //int id = int.Parse(Console.ReadLine());
            //Console.WriteLine("Nouveau nom : ");
            //string nom = Console.ReadLine();

            //Elf e = new Elf(id, nom, true, "plop.jpg");  

            //ElfDAO dao = new ElfDAO();
            //dao.Update(e);

        }

        public static void TestInsertion()
        {
            Console.Write("Nom de l'elfe ? ");
            string nom = Console.ReadLine();
            Elf e = new Elf(0, nom, true, "photo.jpg");
            ElfBusiness bu = new ElfBusiness();
            try
            {
                bu.SaveElf(e);
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
    }
}
